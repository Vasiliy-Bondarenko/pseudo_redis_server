package storage

import "sync"

type ExpiringKeys struct {
	keys []string
	mu sync.RWMutex
}

func (keys *ExpiringKeys) Push(key string) {
	keys.mu.Lock()
	keys.keys = append(keys.keys, key)
	keys.mu.Unlock()
}

func (keys *ExpiringKeys) GetKeys() []string {
	keys.mu.RLock()
	defer keys.mu.RUnlock()
	return keys.keys
}