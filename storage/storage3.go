package storage

// Storage with race protection based on channels
// (implementation incomplete - just to test the idea)
// main part is in ChanneledMap.go

import (
	"github.com/jonboulle/clockwork"
	"errors"
	"time"
)

type Storage3 struct {
	Storage *ChanneledMap
	clock   clockwork.Clock
	Counter int
}

type KV3 struct {
	Key string
	Item Item
}

var storageCounter int

func NewStorage3(clock clockwork.Clock) Storage3 {
	storageCounter++
	storage := Storage3{
		Counter: storageCounter,
	}
	storage.Storage = InitChanneledMap()
	storage.clock = clock
	return storage
}

func (s Storage3) SetItem(key string, item Item) error {
	s.Storage.Store(key, item)
	return nil
}

func (s Storage3) SetString(key string, value string, ttl time.Duration) error {
	s.Storage.Store(key, Item{
		Value_string: value,
		Ttl:          s.makeTtlTime(ttl),
	})
	return nil
}

func (s Storage3) SetList(key string, value []string, ttl time.Duration) error {
	item := Item{
		Value_slice: value,
		Ttl:         s.makeTtlTime(ttl),
	}

	s.Storage.Store(key, item)

	return nil
}

func (s Storage3) SetMap(key string, value map[string]string, ttl time.Duration) error {
	item := Item{
		Value_map: value,
		Ttl:       s.makeTtlTime(ttl),
	}

	s.Storage.Store(key, item)

	return nil
}

func (s Storage3) GetString(key string) (value string, err error) {
	item, err := s.getItem(key)
	return item.Value_string, err
}

func (s Storage3) GetList(key string) (value []string, err error) {
	item, err := s.getItem(key)
	return item.Value_slice, err
}

func (s Storage3) GetMap(key string) (value map[string]string, err error) {
	item, err := s.getItem(key)
	return item.Value_map, err
}

func (s Storage3) GetValueFromList(key string, index int64) (value string, err error) {
	item, err := s.getItem(key)
	slice := item.Value_slice
	return slice[index], err
}

func (s Storage3) GetValueFromMap(key string, index string) (string, error) {
	item, err := s.getItem(key)
	value_map := item.Value_map
	return value_map[index], err
}

func (s Storage3) Remove(key string) error {
	s.Storage.Delete(key)
	return nil
}

func (s Storage3) Keys() (keys []string, err error) {

	s.Storage.Range(func(key string, item Item) bool {
		keys = append(keys, key)
		return true
	})

	return keys, nil
}

func (s Storage3) CleanUpExpired() {
	s.Storage.Range(func(key string, item Item) bool {
		s.itemIsExpired(&item, key)
		return true
	})
}

func (s Storage3) GetRawItemTesting(key string) (Item, bool) {
	value, ok := s.Storage.Load(key)
	if !ok {
		return Item{}, ok
	}

	return value, ok
}

func (s Storage3) GetAllAsMap() map[string]Item {
	m := make(map[string]Item)
	s.Storage.Range(func(key string, item Item) bool {
		m[key] = item
		return true
	})
	return m
}

func (s Storage3) RestoreFromMap(m map[string]Item) {
	for k, v := range m {
		s.Storage.Store(k, v)
	}
}

func (s Storage3) Range(f func(key string, item Item) bool) {
	s.Storage.Range(f)
}

func (s Storage3) itemIsExpired(item *Item, key string) bool {
	if item.expired(s.clock) {
		s.Remove(key)
		return true
	}
	return false
}

func (s Storage3) makeTtlTime(ttl time.Duration) time.Time {
	if ttl == 0 {
		return zeroTime
	}

	return s.clock.Now().Add(ttl)
}

func (s Storage3) getItem(key string) (item Item, err error) {
	item, ok := s.Storage.Load(key)

	if (!ok) {
		return item, errors.New("Not found")
	}

	if s.itemIsExpired(&item, key) {
		return Item{}, errors.New("Not found")
	}

	return item, nil
}
