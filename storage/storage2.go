package storage

import (
	"github.com/jonboulle/clockwork"
	"errors"
	"time"
)

type Storage2 struct {
	Storage   *SyncMap
	ExpiryMap *SyncMap
	clock     clockwork.Clock
}

type KV struct {
	Key  string
	Item Item
}

func NewStorage2(clock clockwork.Clock) Storage2 {
	storage := Storage2{}
	storage.Storage = &SyncMap{}
	storage.ExpiryMap = &SyncMap{}
	storage.clock = clock
	return storage
}

func (s Storage2) SetItem(key string, item Item) error {
	s.Storage.Store(key, item)
	return nil
}

func (s Storage2) SetString(key string, value string, ttl time.Duration) error {
	ExpireAt := s.makeTtlTime(ttl)

	s.saveExpiringTime(ExpireAt, key)

	s.Storage.Store(key, Item{
		Value_string: value,
		Ttl:          ExpireAt,
	})
	return nil
}

func (s Storage2) saveExpiringTime(ExpireAt time.Time, key string) {
	expiringKeys, _ := s.ExpiryMap.LoadOrStore(ExpireAt, &ExpiringKeys{})
	expiringKeys.(*ExpiringKeys).Push(key)
}

func (s Storage2) CleanUpExpired() {
	now := s.clock.Now().Round(time.Second)
	expiredKeys, found := s.ExpiryMap.Load(now)
	if !found {
		return
	}

	keys := expiredKeys.(*ExpiringKeys).GetKeys()

	for _, key := range keys {
		s.Storage.Delete(key)
	}
}

func (s Storage2) SetList(key string, value []string, ttl time.Duration) error {
	item := Item{
		Value_slice: value,
		Ttl:         s.makeTtlTime(ttl),
	}

	s.Storage.Store(key, item)

	return nil
}

func (s Storage2) SetMap(key string, value map[string]string, ttl time.Duration) error {
	item := Item{
		Value_map: value,
		Ttl:       s.makeTtlTime(ttl),
	}

	s.Storage.Store(key, item)

	return nil
}

func (s Storage2) GetString(key string) (value string, err error) {
	item, err := s.getItem(key)
	return item.Value_string, err
}

func (s Storage2) GetList(key string) (value []string, err error) {
	item, err := s.getItem(key)
	return item.Value_slice, err
}

func (s Storage2) GetMap(key string) (value map[string]string, err error) {
	item, err := s.getItem(key)
	return item.Value_map, err
}

func (s Storage2) GetValueFromList(key string, index int64) (value string, err error) {
	item, err := s.getItem(key)
	slice := item.Value_slice
	return slice[index], err
}

func (s Storage2) GetValueFromMap(key string, index string) (string, error) {
	item, err := s.getItem(key)
	value_map := item.Value_map
	return value_map[index], err
}

func (s Storage2) Remove(key string) error {
	s.Storage.Delete(key)
	return nil
}

func (s Storage2) Keys() (keys []string, err error) {

	s.Storage.Range(func(key, value interface{}) bool {
		stringKey := key.(string)
		keys = append(keys, stringKey)
		return true
	})

	return keys, nil
}

func (s Storage2) GetRawItemTesting(key string) (Item, bool) {
	value, ok := s.Storage.Load(key)
	if !ok {
		return Item{}, ok
	}

	return value.(Item), ok
}

func (s Storage2) GetAllAsMap() map[string]Item {
	m := make(map[string]Item)
	s.Storage.Range(func(key, value interface{}) bool {
		stringKey := key.(string)
		valueItem := value.(Item)
		m[stringKey] = valueItem
		return true
	})
	return m
}

func (s Storage2) RestoreFromMap(m map[string]Item) {
	for k, v := range m {
		s.Storage.Store(k, v)
	}
}

func (s Storage2) Range(f func(key, value interface{}) bool) {
	s.Storage.Range(f)
}

func (s Storage2) itemIsExpired(item *Item, key string) bool {
	if item.expired(s.clock) {
		s.Remove(key)
		return true
	}
	return false
}

func (s Storage2) makeTtlTime(ttl time.Duration) time.Time {
	if ttl == 0 {
		return zeroTime
	}

	return s.clock.Now().Add(ttl).Round(time.Second)
}

func (s Storage2) getItem(key string) (item Item, err error) {
	value, ok := s.Storage.Load(key)

	if (!ok) {
		return item, errors.New("Not found")
	}

	item = value.(Item)

	if s.itemIsExpired(&item, key) {
		return Item{}, errors.New("Not found")
	}

	return item, nil
}
