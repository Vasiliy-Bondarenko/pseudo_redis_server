package storage

// Map with race protection based on channels

import "log"

type ChanneledMap struct{
	writeChannel chan func(*store)
	readChannel chan func(*store)
	resChannel chan readResult
	quitChannel chan bool
}

type store map[string]Item

type readResult struct {
	ok bool
	item Item
}

func InitChanneledMap() *ChanneledMap {
	m := ChanneledMap{
		writeChannel: make(chan func(*store)),
		readChannel: make(chan func(*store)),
		resChannel: make(chan readResult),
		quitChannel: make(chan bool),
	}


	go func() {
		var s = make(store)

		for {
			// only one branch is selected at the same time.
			// so only one operation on the storage can be performed concurrently
			select {
			case operator := <-m.writeChannel:
				operator(&s)
			case operator := <-m.readChannel:
				operator(&s)
			case <-m.quitChannel:
				break
			}
		}
	}()

	return &m
}

func (m ChanneledMap) Close() {
	m.quitChannel <- true
}

func (m ChanneledMap) Store(key string, item Item) {
	operator := func(s *store) {
		(*s)[key] = item
	}
	m.writeChannel <- operator
}
func (m ChanneledMap) Delete(key string) {
	operator := func(s *store) {
		delete(*s, key)
	}
	m.writeChannel <- operator
}
func (m ChanneledMap) Load(key string) (Item, bool) {
	operator := func(s *store) {
		item, ok := (*s)[key]
		m.resChannel <- readResult{
			ok: ok,
			item: item,
		}
	}
	m.readChannel <- operator
	readRes := <-m.resChannel
	return readRes.item, readRes.ok
}
func (m ChanneledMap) Range(func(key string, item Item) bool) {
	log.Fatalln("Not implemented yet")
}
