package storage

import (
	"time"
	"github.com/jonboulle/clockwork"
)

// if Ttl of Item set to zeroTime - it will be never expired
var zeroTime = time.Time{}

type Item struct {
	Value_string string
	Value_slice  []string
	Value_map    map[string]string
	Ttl          time.Time
}

func (item Item) expired(clock clockwork.Clock) bool {
	if item.Ttl == zeroTime {
		return false
	}
	return item.Ttl.Before(clock.Now())
}
