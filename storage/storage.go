package storage

import (
	"github.com/jonboulle/clockwork"
	"sync"
	"errors"
	"time"
)

type Storage struct {
	storage map[string]Item
	clock   clockwork.Clock
}

var mutex sync.Mutex

func NewStorage(clock clockwork.Clock) Storage {
	storage := Storage{}
	storage.storage = make(map[string]Item)
	storage.clock = clock
	return storage
}

func (s Storage) SetString(key string, value string, ttl time.Duration) error {
	item := Item{
		Value_string: value,
		Ttl:          s.makeTtlTime(ttl),
	}

	s.setItem(key, item)

	return nil
}

func (s Storage) SetList(key string, value []string, ttl time.Duration) error {
	item := Item{
		Value_slice: value,
		Ttl:         s.makeTtlTime(ttl),
	}

	s.setItem(key, item)

	return nil
}

func (s Storage) SetMap(key string, value map[string]string, ttl time.Duration) error {
	item := Item{
		Value_map: value,
		Ttl:       s.makeTtlTime(ttl),
	}

	s.setItem(key, item)

	return nil
}

func (s Storage) GetString(key string) (value string, err error) {
	item, err := s.getItem(key)
	return item.Value_string, err
}

func (s Storage) GetList(key string) (value []string, err error) {
	item, err := s.getItem(key)
	return item.Value_slice, err
}

func (s Storage) GetMap(key string) (value map[string]string, err error) {
	item, err := s.getItem(key)
	return item.Value_map, err
}

func (s Storage) GetValueFromList(key string, index int64) (value string, err error) {
	item, err := s.getItem(key)
	slice := item.Value_slice
	return slice[index], err
}

func (s Storage) GetValueFromMap(key string, index string) (string, error) {
	item, err := s.getItem(key)
	value_map := item.Value_map
	return value_map[index], err
}

func (s Storage) Remove(key string) error {
	mutex.Lock()
	delete(s.storage, key)
	mutex.Unlock()

	return nil
}

func (s Storage) Keys() (keys []string, err error) {
	mutex.Lock()

	for key := range s.storage {
		keys = append(keys, key)
	}

	mutex.Unlock()
	return keys, nil
}

func (s Storage) CleanUpExpired() {
	for key, item := range s.storage {
		s.itemIsExpired(&item, key)
	}
}

func (s Storage) GetRawItemTesting(key string) (Item, bool) {
	value, ok := s.storage[key]
	return value, ok
}

func (s Storage) itemIsExpired(item *Item, key string) bool {
	if item.expired(s.clock) {
		s.Remove(key)
		return true
	}
	return false
}

func (s Storage) makeTtlTime(ttl time.Duration) time.Time {
	if ttl == 0 {
		return zeroTime
	}

	return s.clock.Now().Add(ttl)
}

func (s Storage) setItem(key string, item Item) {
	mutex.Lock()
	s.storage[key] = item
	mutex.Unlock()
}

func (s Storage) getItem(key string) (item Item, err error) {
	mutex.Lock()
	item, ok := s.storage[key]
	mutex.Unlock()

	if (!ok) {
		return item, errors.New("Not found")
	}

	if s.itemIsExpired(&item, key) {
		return Item{}, errors.New("Not found")
	}

	return item, nil
}
