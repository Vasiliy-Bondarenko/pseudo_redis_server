package gorpc

import (
	"time"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/di"
)

type PingRequest struct{}

type BaseReply struct {
	Error        bool
	ErrorMessage string
}

type SetStringRequest struct {
	Key   string
	Value string
	Ttl   time.Duration
}

type RemoveRequest struct {
	Key string
}

type GetKeysRequest struct{}

type SetListRequest struct {
	Key   string
	Value []string
	Ttl   time.Duration
}

type SetMapRequest struct {
	Key   string
	Value map[string]string
	Ttl   time.Duration
}

type GetRequest struct {
	Key string
}

type GetListItemRequest struct {
	Key   string
	Index int64
}

type GetMapItemRequest struct {
	Key   string
	Index string
}

type SetReply struct {
	BaseReply
}

type RemoveReply struct {
	BaseReply
}

type GetStringReply struct {
	BaseReply
	Value string
}

type GetKeysReply struct {
	BaseReply
	Value []string
}

type GetListItemReply struct {
	BaseReply
	Value string
}

type GetMapItemReply struct {
	BaseReply
	Value string
}

type GetMapReply struct {
	BaseReply
	Value map[string]string
}

type GetListReply struct {
	BaseReply
	Value []string
}

type PsrStorage int

func (this *PsrStorage) Ping(req *PingRequest, reply *string) error {
	*reply = "Pong"
	return nil
}

func (this *PsrStorage) SetString(req *SetStringRequest, reply *SetReply) error {
	di.GetStorage().SetString(req.Key, req.Value, req.Ttl)

	*reply = SetReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
	}
	return nil
}

func (this *PsrStorage) GetString(req *GetRequest, reply *GetStringReply) error {
	value, err := di.GetStorage().GetString(req.Key)

	if err != nil {
		*reply = GetStringReply{
			BaseReply: BaseReply{
				Error:        true,
				ErrorMessage: err.Error(),
			},
			Value: "",
		}
		return nil
	}

	*reply = GetStringReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
		Value: value,
	}
	return nil
}

func (this *PsrStorage) GetList(req *GetRequest, reply *GetListReply) error {
	value, err := di.GetStorage().GetList(req.Key)

	if err != nil {
		*reply = GetListReply{
			BaseReply: BaseReply{
				Error:        true,
				ErrorMessage: err.Error(),
			},
			Value: []string{},
		}
		return nil
	}

	*reply = GetListReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
		Value: value,
	}
	return nil
}

func (this *PsrStorage) SetList(req *SetListRequest, reply *SetReply) error {
	di.GetStorage().SetList(req.Key, req.Value, req.Ttl)

	*reply = SetReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
	}
	return nil
}

func (this *PsrStorage) SetMap(req *SetMapRequest, reply *SetReply) error {
	di.GetStorage().SetMap(req.Key, req.Value, req.Ttl)

	*reply = SetReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
	}
	return nil
}

func (this *PsrStorage) GetMap(req *GetRequest, reply *GetMapReply) error {
	value, err := di.GetStorage().GetMap(req.Key)

	if err != nil {
		*reply = GetMapReply{
			BaseReply: BaseReply{
				Error:        true,
				ErrorMessage: err.Error(),
			},
			Value: map[string]string{},
		}
		return nil
	}

	*reply = GetMapReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
		Value: value,
	}
	return nil
}

func (this *PsrStorage) GetListItem(req *GetListItemRequest, reply *GetListItemReply) error {
	value, err := di.GetStorage().GetValueFromList(req.Key, req.Index)

	if err != nil {
		*reply = GetListItemReply{
			BaseReply: BaseReply{
				Error:        true,
				ErrorMessage: err.Error(),
			},
			Value: "",
		}
		return nil
	}

	*reply = GetListItemReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
		Value: value,
	}
	return nil
}

func (this *PsrStorage) GetMapItem(req *GetMapItemRequest, reply *GetMapItemReply) error {
	value, err := di.GetStorage().GetValueFromMap(req.Key, req.Index)

	if err != nil {
		*reply = GetMapItemReply{
			BaseReply: BaseReply{
				Error:        true,
				ErrorMessage: err.Error(),
			},
			Value: "",
		}
		return nil
	}

	*reply = GetMapItemReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
		Value: value,
	}
	return nil
}

func (this *PsrStorage) GetKeys(req *GetKeysRequest, reply *GetKeysReply) error {
	value, err := di.GetStorage().Keys()

	if err != nil {
		*reply = GetKeysReply{
			BaseReply: BaseReply{
				Error:        true,
				ErrorMessage: err.Error(),
			},
			Value: []string{},
		}
		return nil
	}

	*reply = GetKeysReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
		Value: value,
	}
	return nil
}

func (this *PsrStorage) Remove(req *RemoveRequest, reply *RemoveReply) error {
	di.GetStorage().Remove(req.Key)

	*reply = RemoveReply{
		BaseReply: BaseReply{
			Error:        false,
			ErrorMessage: "",
		},
	}
	return nil
}
