package di

import (
	"github.com/jonboulle/clockwork"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/storage"
	"sync"
)

type diContainer struct {
	storage      storage.Storage2
	configLoaded bool
	mu           sync.RWMutex
}

var container = diContainer {
	storage: storage.NewStorage2(clockwork.NewRealClock()),
	configLoaded: false,
}

func ResetStorage(clock clockwork.Clock) {
	SetStorage(storage.NewStorage2(clock))
}

func ContainerRLock(){
	container.mu.RLock()
}

func ContainerRUnlock(){
	container.mu.RUnlock()
}

func ContainerLock(){
	container.mu.Lock()
}

func ContainerUnlock(){
	container.mu.Unlock()
}

func GetStorage() *storage.Storage2 {
	ContainerLock()
	defer ContainerUnlock()
	return &container.storage
}

func SetStorage(storage storage.Storage2) {
	ContainerLock()
	container.storage = storage
	ContainerUnlock()
}

func MarkLoaded() {
	ContainerLock()
	container.configLoaded = true
	ContainerUnlock()
}

func Loaded() bool {
	ContainerRLock()
	defer ContainerRUnlock()
	return container.configLoaded
}