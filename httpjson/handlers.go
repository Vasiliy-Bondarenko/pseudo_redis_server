package httpjson

import (
	"net/http"
	"encoding/json"
	"log"
)

func InitHandlers(){
	http.HandleFunc("/ping", pong)
}

func pong(w http.ResponseWriter, r *http.Request) {
	encoded, err := json.Marshal(PingResponse{
		Response: "Pong",
		Error:    nil,
	})

	if err != nil {
		log.Println("Failed encoding response. Error: ", err)
	}

	w.Write(encoded)
}
