package httpjson

type PingResponse struct {
	Response string
	Error    error
}