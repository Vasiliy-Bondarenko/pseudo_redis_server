package pool

/*
 * Copyright 2016 DGraph Labs, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import (
	"net/rpc"
)

type PoolGoRpc struct {
	conns chan *rpc.Client
	Addr  string
}

func NewPoolGoRpc(addr string, maxCap int) (*PoolGoRpc, error) {
	p := new(PoolGoRpc)
	p.Addr = addr
	p.conns = make(chan *rpc.Client, maxCap)
	conn, err := p.dialNew()
	if err != nil {
		return p, err
	}
	p.conns <- conn
	return p, nil
}

func (p *PoolGoRpc) dialNew() (*rpc.Client, error) {
	return rpc.Dial("tcp", p.Addr)
}

func (p *PoolGoRpc) Get() (*rpc.Client, error) {
	select {
	case conn := <-p.conns:
		return conn, nil
	default:
		return p.dialNew()
	}
}

func (p *PoolGoRpc) Put(conn *rpc.Client) error {
	select {
	case p.conns <- conn:
		return nil
	default:
		return conn.Close()
	}
}
