package tests

import (
	"testing"
	"github.com/spf13/viper"
	"log"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/prclient"
	"time"
	"github.com/icrowley/fake"
	"math/rand"
	"github.com/forestgiant/sliceutil"
	"reflect"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/di"
	"github.com/jonboulle/clockwork"
)

func getConnectedClient(connections int) prclient.PrGoRpcClient {
	host := viper.GetString("server.gorpc:gob.host:port")
	cp, err := prclient.New(host, connections)
	if err != nil {
		log.Fatal("Can't connect to the server, Error: ", err)
	}
	return cp
}

func TestFailedConnection(t *testing.T) {
	_, err := prclient.New("localhost:99999", 1)
	if err == nil {
		log.Fatal("Must return error. Gon nil")
	}
}

func TestClientPing(t *testing.T) {
	client := getConnectedClient(1)

	res, err := client.Ping()
	if res != "Pong" {
		log.Fatal("Ping must return 'Pong', got: ", res)
	}
	if err != nil {
		log.Fatal("Ping must return 'Pong', got error: ", err)
	}
}

func TestClientSetGetString(t *testing.T) {
	client := getConnectedClient(1)
	key := fake.Word()
	value := fake.Sentence()

	err := client.SetString(key, value, time.Minute)
	if err != nil {
		log.Fatal("SetString() failed, got error: ", err)
	}

	reply, err := client.GetString(key)
	if err != nil {
		log.Fatal("GetString() failed, got error: ", err)
	}
	if reply != value {
		log.Fatalf("GetString() failed, expected: '%v' got: '%v'", value, reply)
	}
}

func TestClientRemove(t *testing.T) {
	client := getConnectedClient(1)
	key := fake.Word()
	value := fake.Sentence()

	err := client.SetString(key, value, time.Minute)

	err = client.Remove(key)
	if err != nil {
		log.Fatal("Remove() failed, got error: ", err)
	}

	reply, err := client.GetString(key)
	if err == nil {
		log.Fatal("Remove() failed. It must return error.")
	}
	if reply != "" {
		log.Fatalf("Remove() failed, expected: nil got: '%v'", reply)
	}
}

func TestClientSetGetList(t *testing.T) {
	client := getConnectedClient(1)
	key := fake.Word()
	list := fakeList()

	err := client.SetList(key, list, time.Minute)
	if err != nil {
		log.Fatal("SetList() failed, got error: ", err)
	}

	reply, err := client.GetList(key)
	if err != nil {
		log.Fatal("GetList() failed, got error: ", err)
	}
	if !sliceutil.Compare(reply, list) {
		log.Fatalf("GetList() failed, expected: '%v' got: '%v'", list, reply)
	}
}

func TestClientSetGetMap(t *testing.T) {
	client := getConnectedClient(1)
	key := fake.Word()
	valueMap := fakeMap()

	err := client.SetMap(key, valueMap, time.Minute)
	if err != nil {
		log.Fatal("SetMap() failed, got error: ", err)
	}

	reply, err := client.GetMap(key)
	if err != nil {
		log.Fatal("GetMap() failed, got error: ", err)
	}
	if !reflect.DeepEqual(reply, valueMap) {
		log.Fatalf("GetMap() failed, expected: '%v' got: '%v'", valueMap, reply)
	}
}

func TestClientGetValueFromList(t *testing.T) {
	client := getConnectedClient(1)
	key := fake.Word()
	specialValue := fake.Sentence()
	list := []string{"zero", specialValue, "two"}

	err := client.SetList(key, list, time.Minute)

	reply, err := client.GetListItem(key, 1)
	if err != nil {
		log.Fatal("GetListItem() failed, got error: ", err)
	}
	if reply != specialValue {
		log.Fatalf("GetListItem() failed, expected: '%v' got: '%v'", specialValue, reply)
	}
}

func TestClientGetValueFromMap(t *testing.T) {
	client := getConnectedClient(1)
	key := fake.Word()
	index := fake.Word()
	value := fake.Word()
	valueMap := fakeMap()
	valueMap[index] = value

	err := client.SetMap(key, valueMap, time.Minute)

	reply, err := client.GetMapItem(key, index)
	if err != nil {
		log.Fatal("GetValueFromMap() failed, got error: ", err)
	}
	if !reflect.DeepEqual(reply, value) {
		log.Fatalf("GetValueFromMap() failed, expected: '%v' got: '%v'", value, reply)
	}
}

func TestClientGetKeys(t *testing.T) {
	client := getConnectedClient(20)
	di.ResetStorage(clockwork.NewFakeClock())

	a := fake.Word()
	b := fake.Word()
	client.SetString(a, fake.Word(), time.Minute)
	client.SetString(b, fake.Word(), time.Minute)

	reply, err := client.Keys()
	if err != nil {
		log.Fatal("Keys() failed, got error: ", err)
	}
	expected := []string{a, b}
	if !sliceutil.Compare(expected, reply) {
		log.Fatalf("Keys() failed, expected: %v got: %v", expected, reply)
	}
}

func fakeList() (list[]string) {
	for i:=1; i<=rand.Intn(100); i++ {
		list = append(list, fake.Word())
	}
	return list
}

func fakeMap() (map[string]string) {
	rez := make(map[string]string, 10)
	for i:=1; i<=10; i++ {
		rez[fake.Word()] = fake.Sentence()
	}
	return rez
}
