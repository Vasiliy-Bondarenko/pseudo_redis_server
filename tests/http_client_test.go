package tests

import (
	"testing"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/prclient"
	"github.com/spf13/viper"
)

func TestPing(t *testing.T) {
	host := viper.GetString("server.http:json.host:port")
	client := prclient.NewHttpClient(host, 10)
	res, err := client.Ping()
	if err != nil {
		t.Fatal("Ping should not return errors. Got: ", err)
	}
	if res != "Pong" {
		t.Fatal("Ping should return 'Pong'. Got: ", res)
	}
}