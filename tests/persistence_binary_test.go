package tests

import (
	"testing"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/persistence"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/storage"
	"github.com/jonboulle/clockwork"
	"time"
)

func TestBinaryReadWrite(t *testing.T) {
	originalStorage := storage.NewStorage2(clockwork.NewRealClock())
	originalStorage.SetString("FIRST\nKEY", "VALUE\n1", time.Hour)

	filename := "binary.db"
	snapshotter := persistence.NewBinarySnapshotter(filename)
	err := snapshotter.Write(&originalStorage)
	if err != nil {
		t.Fatalf("Failed writing to disk: %v", err)
	}

	persistedStorage, err := snapshotter.Read()
	if err != nil {
		t.Fatalf("Failed reading from disk: %v", err)
	}

	value, err := persistedStorage.GetString("FIRST\nKEY")
	if err != nil {
		t.Fatalf("Failed restoring value from restored map. Error: %v", err)
	}
	if value != "VALUE\n1" {
		t.Fatalf("Maps are not equal. Original: %v, persisted: %v", originalStorage, persistedStorage)
	}
}
