package tests

import (
	"testing"
	"github.com/jonboulle/clockwork"
	"github.com/forestgiant/sliceutil"
	"sync"
	"time"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/storage"
)

func getStorage2() (storage.Storage2, clockwork.FakeClock) {
	var fakeClock = clockwork.NewFakeClock()
	return storage.NewStorage2(fakeClock), fakeClock
}

func TestStorage2ReadWriteDelete(t *testing.T) {
	s, _ := getStorage2()

	s.SetString("one", "str1", 0)
	actual, err := s.GetString("one")
	if expected := "str1"; actual != expected || err != nil {
		t.Errorf("failed setting string value. invalid value %+v returned, expected %+v", actual, expected)
	}

	s.SetString("two", "str2", 0)
	actual, err = s.GetString("two")
	if expected := "str2"; actual != expected || err != nil {
		t.Errorf("failed setting string value. invalid value %+v returned, expected %+v", actual, expected)
	}

	s.Remove("one")
	if _, err := s.GetString("one"); err == nil {
		t.Errorf("failed removing value.")
	}
}

func TestStorage2Keys(t *testing.T) {
	s, _ := getStorage2()

	s.SetString("one", "str1", 0)
	s.SetString("two", "str2", 0)

	expected_keys := []string{"one", "two"}
	actual_keys, _ := s.Keys()

	if (!sliceutil.Compare(expected_keys, actual_keys)) { // compare ignoring order!
		t.Errorf("failed getting storage Keys. %v returned, expected %v", actual_keys, expected_keys)
	}
}

func TestStorage2TTL(t *testing.T) {
	s, fakeClock := getStorage2()

	key := "one"
	s.SetString(key, "str", time.Minute*5)

	fakeClock.Advance(time.Minute * 4)

	actual, err := s.GetString(key)
	if expected := "str"; expected != actual || err != nil {
		t.Errorf("asserting value with TTL before timeout. | returned: %+v, expected: %+v", actual, expected)
	}

	fakeClock.Advance(time.Minute * 2)

	_, err = s.GetString(key)
	if nil == err {
		t.Errorf("GetString must return an error after TTL timed out")
	}
}

func TestStorage2SliceAsList(t *testing.T) {
	s, _ := getStorage2()

	expected := []string{"str", "other str"}
	s.SetList("slice", expected, 0)

	actual, _ := s.GetList("one")
	if sliceutil.Compare(actual, expected) {
		t.Errorf("asserting value with TTL before timeout. | returned: %+v, expected: %+v", actual, expected)
	}

	actual_string, _ := s.GetValueFromList("slice", 1)

	if expected := "other str"; expected != actual_string {
		t.Errorf("GetValueFromList() failed. | returned: %+v, expected: %+v", actual_string, expected)
	}
}

func TestStorage2Maps(t *testing.T) {
	s, _ := getStorage2()

	// given
	var testMap = map[string]string{
		"other": "value other",
		"str":   "value string",
	}
	s.SetMap("map", testMap, 0)

	// when
	actual, err := s.GetMap("map")

	// then
	if err != nil {
		t.Errorf("GetMap() failed. error: '%v'", err.Error())
	}
	if len(testMap) != len(actual) || actual["other"] != "value other" || actual["str"] != "value string" {
		t.Errorf("GetMap() failed. invalid value '%v' returned, expected '%v'", actual, testMap)
	}

	// when
	value, err := s.GetValueFromMap("map", "str")

	// then
	if err != nil {
		t.Errorf("GetValueFromMap() failed. error: '%v'", err.Error())
	}

	if expected := "value string"; value != expected {
		t.Errorf("GetValueFromMap() failed. invalid value '%v' returned, expected '%v'", value, expected)
	}
}

func TestStorage2CleanupExpiredMethod(t *testing.T) {
	s, fakeClock := getStorage2()

	// given
	s.SetString("key", "value", time.Minute*5)
	fakeClock.Advance(time.Minute * 4)

	// Sanity check
	s.CleanUpExpired()
	if _, ok := s.GetRawItemTesting("key"); !ok {
		t.Errorf("asserting value still exists in storage after cleaunp before item is expired")
	}

	// real action
	fakeClock.Advance(time.Minute*1 + 1)
	s.CleanUpExpired()

	if _, ok := s.GetRawItemTesting("key"); ok {
		t.Errorf("asserting value NOT exists in storage after item is expired after cleaunp")
	}
}

/* TODO: i tested only simultaneous read-write safety here.
also random deleting-reading-writing must be tested.
it can be tested in the same way as shown here, so it is skipped.
 */
func TestStorage2ThreadSafety(t *testing.T) {
	s, _ := getStorage2()

	const itemsInStorage = 1000
	const workers = 100 // up the number for better testing

	var waitGroup sync.WaitGroup
	keys := makeKeys(itemsInStorage)

	prepareStorage2(&s, keys)

	// changing the storage many times
	waitGroup.Add(workers)
	for i := 1; i <= workers; i++ {
		go writeToStorage2(&s, t, keys, &waitGroup)
	}

	// and at the same time reading from storage
	waitGroup.Add(workers)
	for i := 1; i <= workers; i++ {
		go readFromStorage2(&s, t, keys, &waitGroup)
	}

	waitGroup.Wait()
}

func prepareStorage2(storage *storage.Storage2, keys map[string]string) {
	for k, v := range keys {
		storage.SetString(k, v, 0)
	}
}

func prepareStorage3(storage *storage.Storage3, keys map[string]string) {
	for k, v := range keys {
		storage.SetString(k, v, 0)
	}
}

func writeToStorage2(storage *storage.Storage2, t *testing.T, keys map[string]string, waitGroup *sync.WaitGroup) {
	defer waitGroup.Done()
	for k, _ := range keys {
		storage.SetString(k, "10", 0)
	}
}

func readFromStorage2(storage *storage.Storage2, t *testing.T, keys map[string]string, waitGroup *sync.WaitGroup) {
	defer waitGroup.Done()
	for k, _ := range keys {
		_, err := storage.GetString(k)

		if err != nil {
			t.Errorf("Failed reading from storage")
		}
	}
}
