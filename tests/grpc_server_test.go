package tests

import (
	"testing"
	"golang.org/x/net/context"
	"github.com/forestgiant/sliceutil"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/pr_grpc"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/di"
	"github.com/jonboulle/clockwork"
)

func TestConnectionAndPing(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	r, err := testGrpcClient.Ping(context.Background(), &pr_grpc.PingRequest{})

	// assertions
	if err != nil {
		t.Errorf("Ping() failed. error: '%v'", err.Error())
	}

	if expected := "Pong"; r.Message != expected {
		t.Errorf("Ping() failed. '%v' returned, expected '%v'", r.Message, expected)
	}
}

func TestSetGetString(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	setStringReply, err := testGrpcClient.SetString(context.Background(), &pr_grpc.SetStringRequest{
		Key:          "Key",
		Value:        "Value",
		TtlInSeconds: 10,
	})

	// assertions
	if err != nil {
		t.Fatalf("SetString() failed. error: '%v'", err.Error())
	}

	if setStringReply.Error != false {
		t.Fatalf("SetString() failed. Error returned")
	}

	if setStringReply.ErrorMessage != "" {
		t.Fatalf("SetString() failed. ErrorMessage '%v' returned", setStringReply.ErrorMessage)
	}

	// action
	getStringReply, err := testGrpcClient.GetString(context.Background(), &pr_grpc.GetStringRequest{Key: "Key"})

	// assertions
	if err != nil {
		t.Fatalf("GetString() failed. error: '%v'", err.Error())
	}

	if getStringReply.Error != false {
		t.Fatalf("GetString() failed. Error returned")
	}

	if getStringReply.ErrorMessage != "" {
		t.Fatalf("GetString() failed. ErrorMessage '%v' returned", getStringReply.ErrorMessage)
	}

	if expected := "Value"; getStringReply.Value != expected {
		t.Errorf("GetString() failed. Value '%v' returned, '%v' expected", getStringReply.Value, expected)
	}
}

func TestGetStringError(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	getStringReply, err := testGrpcClient.GetString(context.Background(), &pr_grpc.GetStringRequest{Key: "Not Existing Key"})

	// assertions
	if err != nil {
		t.Fatalf("GetString() faild, while it shoud return pritty reply with error message")
	}

	if getStringReply.Error != true {
		t.Fatalf("GetString() must return Error = true")
	}

	if getStringReply.ErrorMessage == "" {
		t.Fatalf("GetString() must return ErrorMessage, empty string returned")
	}

	if expected := ""; getStringReply.Value != expected {
		t.Errorf("GetString() failed. Value '%v' returned, '%v' expected", getStringReply.Value, expected)
	}
}

func TestSetGetList(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	setListReply, err := testGrpcClient.SetList(context.Background(), &pr_grpc.SetListRequest{
		Key:          "List",
		Value:        []string{"1", "2"},
		TtlInSeconds: 10,
	})

	// assertions
	if err != nil {
		t.Fatalf("SetList() failed. error: '%v'", err.Error())
	}

	if setListReply.Error != false {
		t.Fatalf("SetList() failed. Error returned")
	}

	if setListReply.ErrorMessage != "" {
		t.Fatalf("SetList() failed. ErrorMessage '%v' returned", setListReply.ErrorMessage)
	}

	// action
	getListReply, err := testGrpcClient.GetList(context.Background(), &pr_grpc.GetListRequest{Key: "List"})

	// assertions
	if err != nil {
		t.Fatalf("GetList() failed. error: '%v'", err.Error())
	}

	if getListReply.Error != false {
		t.Fatalf("GetList() failed. Error returned")
	}

	if getListReply.ErrorMessage != "" {
		t.Fatalf("GetList() failed. ErrorMessage '%v' returned", getListReply.ErrorMessage)
	}

	if expected := []string{"1", "2"}; ! sliceutil.Compare(getListReply.Value, expected) {
		t.Errorf("GetList() failed. Value '%v' returned, '%v' expected", getListReply.Value, expected)
	}
}

func TestGetListError(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	getListReply, err := testGrpcClient.GetList(context.Background(), &pr_grpc.GetListRequest{Key: "Not Existing List"})

	// assertions
	if err != nil {
		t.Fatalf("GetList() failed. error: '%v'", err.Error())
	}

	if getListReply.Error != true {
		t.Fatalf("GetList() must fail with error.")
	}

	if getListReply.ErrorMessage == "" {
		t.Fatalf("GetList() must failed with messsage")
	}

	if expected := []string{}; ! sliceutil.Compare(getListReply.Value, expected) {
		t.Errorf("GetList() Value '%v' returned, '%v' expected", getListReply.Value, expected)
	}
}

func TestSetGetMap(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	mapValue := map[string]string{"First": "1", "Second": "2"}
	setMapReply, err := testGrpcClient.SetMap(context.Background(), &pr_grpc.SetMapRequest{
		Key:          "Map",
		Value:        mapValue,
		TtlInSeconds: 10,
	})

	// assertions
	if err != nil {
		t.Fatalf("SetMap() failed. error: '%v'", err.Error())
	}

	if setMapReply.Error != false {
		t.Fatalf("SetMap() failed. Error returned")
	}

	if setMapReply.ErrorMessage != "" {
		t.Fatalf("SetMap() failed. ErrorMessage '%v' returned", setMapReply.ErrorMessage)
	}

	// action
	getMapReply, err := testGrpcClient.GetMap(context.Background(), &pr_grpc.GetMapRequest{Key: "Map"})

	// assertions
	if err != nil {
		t.Fatalf("GetMap() failed. error: '%v'", err.Error())
	}

	if getMapReply.Error != false {
		t.Fatalf("GetMap() failed. Error returned")
	}

	if getMapReply.ErrorMessage != "" {
		t.Fatalf("GetMap() failed. ErrorMessage '%v' returned", getMapReply.ErrorMessage)
	}

	expected := mapValue
	actual := getMapReply.Value
	if len(expected) != len(actual) || actual["First"] != "1" || actual["Second"] != "2" {
		t.Errorf("GetMap() failed. Value '%v' returned, '%v' expected", getMapReply.Value, expected)
	}
}

func TestGetMapError(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	getMapReply, err := testGrpcClient.GetMap(context.Background(), &pr_grpc.GetMapRequest{Key: "Not Existing Map"})

	// assertions
	if err != nil {
		t.Fatalf("GetMap() failed. error: '%v'", err.Error())
	}

	if getMapReply.Error != true {
		t.Fatalf("GetMap() must fail with error.")
	}

	if getMapReply.ErrorMessage == "" {
		t.Fatalf("GetMap() must failed with messsage")
	}

	if len(getMapReply.Value) != 0 {
		t.Errorf("GetMap() Value '%v' returned, empty map[string]string expected", getMapReply.Value)
	}
}

func TestGetValueFromList(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	_, err := testGrpcClient.SetList(context.Background(), &pr_grpc.SetListRequest{
		Key:          "List",
		Value:        []string{"1", "2"},
		TtlInSeconds: 10,
	})

	// action
	reply, err := testGrpcClient.GetValueFromList(context.Background(), &pr_grpc.GetValueFromListRequest{Key: "List", Index: 1})

	// assertions
	if err != nil {
		t.Fatalf("GetValueFromList() failed. error: '%v'", err.Error())
	}

	if reply.Error != false {
		t.Fatalf("GetValueFromList() failed. Error returned")
	}

	if reply.ErrorMessage != "" {
		t.Fatalf("GetValueFromList() failed. ErrorMessage '%v' returned", reply.ErrorMessage)
	}

	if expected := "2"; reply.Value != expected {
		t.Errorf("GetValueFromList() failed. Value '%v' returned, '%v' expected", reply.Value, expected)
	}
}

func TestGetValueFromMap(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	_, err := testGrpcClient.SetMap(context.Background(), &pr_grpc.SetMapRequest{
		Key:          "Map",
		Value:        map[string]string{"First": "1", "Second": "2"},
		TtlInSeconds: 10,
	})

	// action
	reply, err := testGrpcClient.GetValueFromMap(context.Background(), &pr_grpc.GetValueFromMapRequest{Key: "Map", Index: "Second"})

	// assertions
	if err != nil {
		t.Fatalf("GetValueFromMap() failed. error: '%v'", err.Error())
	}

	if reply.Error != false {
		t.Fatalf("GetValueFromMap() failed. Error returned")
	}

	if reply.ErrorMessage != "" {
		t.Fatalf("GetValueFromMap() failed. ErrorMessage '%v' returned", reply.ErrorMessage)
	}

	if expected := "2"; reply.Value != expected {
		t.Errorf("GetValueFromMap() failed. Value '%v' returned, '%v' expected", reply.Value, expected)
	}
}

func TestRemove(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	// action
	testGrpcClient.SetString(context.Background(), &pr_grpc.SetStringRequest{
		Key:          "Key",
		Value:        "Value",
		TtlInSeconds: 10,
	})

	deleteReply, err := testGrpcClient.Remove(context.Background(), &pr_grpc.RemoveRequest{
		Key: "Key",
	})

	// assertions
	if err != nil {
		t.Fatalf("Remove() failed. error: '%v'", err.Error())
	}

	if deleteReply.Error != false {
		t.Fatalf("Remove() failed. Error returned")
	}

	if deleteReply.ErrorMessage != "" {
		t.Fatalf("Remove() failed. ErrorMessage '%v' returned", deleteReply.ErrorMessage)
	}

	// action
	getStringReply, err := testGrpcClient.GetString(context.Background(), &pr_grpc.GetStringRequest{Key: "Key"})

	// assertions
	if err != nil {
		t.Fatalf("GetString() faild, while it shoud return pritty reply with error message")
	}

	if getStringReply.Error != true {
		t.Fatalf("GetString() must return Error = true")
	}

	if getStringReply.ErrorMessage == "" {
		t.Fatalf("GetString() must return ErrorMessage, empty string returned")
	}

	if expected := ""; getStringReply.Value != expected {
		t.Errorf("GetString() failed. Value '%v' returned, '%v' expected", getStringReply.Value, expected)
	}
}

func TestGetKeys(t *testing.T) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()
	di.ResetStorage(clockwork.NewFakeClock())

	// action
	testGrpcClient.SetString(context.Background(), &pr_grpc.SetStringRequest{
		Key:          "Key",
		Value:        "Value",
		TtlInSeconds: 10,
	})

	testGrpcClient.SetString(context.Background(), &pr_grpc.SetStringRequest{
		Key:          "Other",
		Value:        "Value",
		TtlInSeconds: 10,
	})

	// action
	reply, err := testGrpcClient.Keys(context.Background(), &pr_grpc.KeysRequest{})

	// assertions
	if err != nil {
		t.Fatalf("Keys() failed. error: '%v'", err.Error())
	}

	if reply.Error != false {
		t.Fatalf("Keys() failed. Error returned")
	}

	if reply.ErrorMessage != "" {
		t.Fatalf("Keys() failed. ErrorMessage '%v' returned", reply.ErrorMessage)
	}

	if expected := []string{"Key", "Other"}; !sliceutil.Compare(reply.Keys, expected) {
		t.Errorf("Keys() failed. Value '%v' returned, '%v' expected", reply.Keys, expected)
	}
}
