package tests

import (
	"testing"
	"time"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/gorpc"
)

func TestServerPing(t *testing.T) {
	client := getGoRpcClient()

	var reply string
	err := client.Call("PsrStorage.Ping", gorpc.PingRequest{}, &reply)

	if err != nil {
		t.Fatal("Client call error:", err)
	}
	if reply != "Pong" {
		t.Fatal("Wrong result received: ", reply)
	}
}

func TestServerSetGet(t *testing.T) {
	client := getGoRpcClient()

	// action
	req := gorpc.SetStringRequest{
		Key:   "key",
		Value: "value",
		Ttl: time.Minute,
	}
	var reply gorpc.SetReply
	err := client.Call("PsrStorage.SetString", req, &reply)

	// assertions
	if err != nil {
		t.Fatal("Client call error:", err)
	}
	if reply.Error != false || reply.ErrorMessage != "" {
		t.Fatal("Wrong result received: ", reply)
	}

	// action
	var getStringReply gorpc.GetStringReply
	err = client.Call("PsrStorage.GetString", gorpc.GetRequest{Key: "key"}, &getStringReply)

	// assertions
	if err != nil {
		t.Fatal("Client call error:", err)
	}
	if reply.Error != false || reply.ErrorMessage != "" {
		t.Fatal("Wrong result received: ", reply)
	}
	if getStringReply.Value != "value" {
		t.Fatal("Wrong result received: ", getStringReply.Value)
	}
}
