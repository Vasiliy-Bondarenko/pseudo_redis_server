package tests

import (
	"testing"
	"os"
	"time"
	"github.com/spf13/viper"
	"log"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/pr_grpc"
	"google.golang.org/grpc"
	"context"
	"net/rpc"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/core"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/di"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/prclient"
)

func TestMain(m *testing.M) {
	go core.Start()

	// wait for config to load
	for di.Loaded() != true {
		log.Println("Waiting for config to start...")
		time.Sleep(time.Millisecond * 50)
	}

	// wait for http server to start
	host := viper.GetString("server.http:json.host:port")
	client := prclient.NewHttpClient(host, 1)
	for {
		pong, err := client.Ping()
		if pong == "Pong" {
			break
		}
		log.Printf("Waiting for HTTP to start... Received: '%v', Error: '%v'", pong, err)
		time.Sleep(time.Millisecond * 200)
	}

	os.Exit(m.Run())
}

func getGrpcClient() (pr_grpc.PseudoRedisServiceClient, *grpc.ClientConn) {
	// Set up a connection to the server.
	// docs: https://github.com/grpc/grpc-go/blob/master/examples/gotutorial.md
	var err error
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	testGrpcClientConnection, err := grpc.DialContext(
		ctx,
		viper.GetString("server.grpc:protobuf.host:port"),
		grpc.WithInsecure(),
		grpc.WithBlock(),
	)

	if err != nil {
		log.Fatalf("Can't connect: %v. Run the server before running this set of tests!", err)
	}

	testGrpcClient := pr_grpc.NewPseudoRedisServiceClient(testGrpcClientConnection)

	return testGrpcClient, testGrpcClientConnection
}

func getGoRpcClient() (*rpc.Client) {
	host := viper.GetString("server.gorpc:gob.host:port")
	client, err := rpc.Dial("tcp", host)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	return client
}
