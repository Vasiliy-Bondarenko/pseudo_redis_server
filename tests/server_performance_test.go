package tests

import (
	"testing"
	"github.com/go-redis/redis"
	"log"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/pr_grpc"
	"context"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/pool"
	"github.com/spf13/viper"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/gorpc"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/prclient"
	"runtime"
	"net/http"
	"io/ioutil"
	"io"
)

var httpClient prclient.HttpClient

func BenchmarkHttpServerPing(b *testing.B) {
	host := viper.GetString("server.http:json.host:port")
	// https://stackoverflow.com/questions/39813587/go-client-program-generates-a-lot-a-sockets-in-time-wait-state
	http.DefaultTransport.(*http.Transport).MaxIdleConnsPerHost = runtime.GOMAXPROCS(0) + 1 // i don't understand why i need "+ 1" here, but it makes benchmark run faster :)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {

			res, err := http.Get("http://localhost" + host + "/ping")
			if err != nil {
				b.Fatalf("Call error: %v", err)
			}
			responseData, err := ioutil.ReadAll(res.Body)
			io.Copy(ioutil.Discard, res.Body)
			res.Body.Close()
			if err != nil {
				log.Fatal(err)
			}
			pong := string(responseData)
			if pong != `{"Response":"Pong","Error":null}` {
				b.Fatalf("Wrong result received: %v, response: %v", err, pong)
			}
		}
	})
}

func BenchmarkHttpClientPing(b *testing.B) {
	nilClient := prclient.HttpClient{}
	if httpClient == nilClient {
		host := viper.GetString("server.http:json.host:port")
		maxIdleConns := runtime.GOMAXPROCS(0)
		httpClient = prclient.NewHttpClient("localhost"+host, maxIdleConns)
	}

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {

			pong, err := httpClient.Ping()
			if err != nil {
				b.Fatalf("Call error: %v, reponse: %v", err, pong)
			}
			if pong != "Pong" {
				b.Fatalf("Wrong result received: %v, reponse: %v", err, pong)
			}
		}
	})
}

func BenchmarkGoRpcClientPool(b *testing.B) {
	client := getConnectedClient(50)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {

			reply, err := client.Ping()
			if err != nil {
				log.Fatal("Ping error:", err)
			}
			if reply != "Pong" {
				b.Fatal("Wrong result received")
			}

		}
	})
}

// тратит время на установку нового соединения во время запроса?
func BenchmarkGoRpcClientPoolLimitedTo1(b *testing.B) {
	client := getConnectedClient(1)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {

			reply, err := client.Ping()
			if err != nil {
				log.Fatal("Ping error:", err)
			}
			if reply != "Pong" {
				b.Fatal("Wrong result received")
			}

		}
	})
}

func BenchmarkGoRpcPoolPing(b *testing.B) {
	cp, _ := pool.NewPoolGoRpc(viper.GetString("server.gorpc:gob.host:port"), 50)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		client, _ := cp.Get()
		for pb.Next() {

			args := gorpc.PingRequest{}
			var reply string
			err := client.Call("PsrStorage.Ping", args, &reply)
			if err != nil {
				log.Fatal("Ping error:", err)
			}
			if reply != "Pong" {
				b.Fatal("Wrong result received")
			}

		}
		cp.Put(client)
	})
}

// использует одно и то же соединение?..
func BenchmarkGoRpcSingleConnectionPing(b *testing.B) {
	client := getGoRpcClient()

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {

			// Synchronous call
			var reply string
			err := client.Call("PsrStorage.Ping", gorpc.PingRequest{}, &reply)
			if err != nil {
				log.Fatal("Call error:", err)
			}
			if reply != "Pong" {
				b.Fatal("Wrong result received")
			}

		}
	})
}

func BenchmarkRealRedisPoolPing(b *testing.B) {
	realRedisPing(b, 20)
}

func BenchmarkRealRedisSingleConnectionPing(b *testing.B) {
	realRedisPing(b, 1)
}

func BenchmarkGrpcWithPoolPing(b *testing.B) {
	gprcWithPoolAction(b, func(client pr_grpc.PseudoRedisServiceClient, b *testing.B) {
		grpcPing(client, b)
	})
}

func BenchmarkGrpcSingleConnectionPing(b *testing.B) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			grpcPing(testGrpcClient, b)
		}
	})
}

func BenchmarkRealRedisPoolGetKeyValue(b *testing.B) {
	presaveRedisKeyValuePair("key", "value")

	action := func(client *redis.Client) {
		val, err := client.Get("key").Result()
		if err != nil {
			panic(err)
		}

		if err != nil {
			b.Errorf("Failed reading from storage")
		}

		if val != "value" {
			b.Errorf("Wrong value received")
		}
	}

	realRedisAction(20, b, action)
}

func BenchmarkGrpcGetKeyValue(b *testing.B) {
	testGrpcClient, connection := getGrpcClient()
	defer connection.Close()

	testGrpcClient.SetString(context.Background(), &pr_grpc.SetStringRequest{
		Key:          "Key",
		Value:        "Value",
		TtlInSeconds: 0,
	})

	gprcWithPoolAction(b, func(client pr_grpc.PseudoRedisServiceClient, b *testing.B) {
		value, err := client.GetString(context.Background(), &pr_grpc.GetStringRequest{
			Key: "Key",
		})

		if err != nil {
			b.Errorf("Failed reading from storage. Error: %v", err)
		}

		if value.Value != "Value" {
			b.Errorf("Failed reading from storage. Got: '%v', expected: 'Value'", value.Value)
			b.Errorf("Whole reply: %v", value)
		}
	})
}

func gprcWithPoolAction(b *testing.B, action func(client pr_grpc.PseudoRedisServiceClient, b *testing.B)) {

	cp := pool.NewPoolGrpc(viper.GetString("server.grpc:protobuf.host:port"), 20)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		connection, _ := cp.Get()
		client := pr_grpc.NewPseudoRedisServiceClient(connection)

		for pb.Next() {
			action(client, b)
		}

		cp.Put(connection)
	})
}

func presaveRedisKeyValuePair(key, value string) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer client.Close()
	err := client.Set(key, value, 0).Err()
	if err != nil {
		panic(err)
	}
}

func realRedisPing(b *testing.B, poolSize int) {
	action := func(client *redis.Client) {
		val, err := client.Ping().Result()
		if err != nil {
			panic(err)
		}

		if err != nil {
			b.Errorf("Failed reading from storage")
		}

		if val != "PONG" {
			b.Errorf("Wrong value received")
		}
	}
	realRedisAction(poolSize, b, action)
}

func realRedisAction(poolSize int, b *testing.B, action func(*redis.Client)) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
		PoolSize: poolSize,
	})
	defer client.Close()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			action(client)
		}
	})
}

func grpcPing(client pr_grpc.PseudoRedisServiceClient, b *testing.B) {
	reply, err := client.Ping(context.Background(), &pr_grpc.PingRequest{})
	if err != nil {
		b.Errorf("Failed reading from storage")
	}
	if reply.Message != "Pong" {
		b.Errorf("Failed receiving Pong")
	}
}
