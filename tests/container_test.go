package tests

import (
	"testing"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/di"
)

func TestContainer(t *testing.T) {
	storage := di.GetStorage()
	storage.SetString("key", "new value", 0)
	value, err := storage.GetString("key")
	if err != nil {
		t.Errorf("Some error happend", err.Error())
	}
	if value != "new value" {
		t.Errorf("Storage must return 'new value', got '%v'", value)
	}

	value, err = di.GetStorage().GetString("key")
	if err != nil {
		t.Errorf("Some error happend", err.Error())
	}
	if value != "new value" {
		t.Errorf("Storage must return 'new value', got '%v'", value)
	}
}
