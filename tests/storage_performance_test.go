package tests

import (
	"testing"
)

func BenchmarkDirectAccessStorageWith1000Elements(b *testing.B) {
	const itemsInStorage = 1000
	testStoragePerformance(itemsInStorage, b)
}

func BenchmarkDirectAccessStorageWith1000000Elements(b *testing.B) {
	const itemsInStorage = 1000 * 1000
	testStoragePerformance(itemsInStorage, b)
}

func BenchmarkDirectAccessStorage2With1000Elements(b *testing.B) {
	const itemsInStorage = 1000
	testStorage2Performance(itemsInStorage, b)
}

func BenchmarkDirectAccessStorage2With1000000Elements(b *testing.B) {
	const itemsInStorage = 1000 * 1000
	testStorage2Performance(itemsInStorage, b)
}

func BenchmarkDirectAccessStorage3With1000Elements(b *testing.B) {
	const itemsInStorage = 1000
	testStorage3Performance(itemsInStorage, b)
}

func BenchmarkDirectAccessStorage3With1000000Elements(b *testing.B) {
	const itemsInStorage = 1000 * 1000
	testStorage3Performance(itemsInStorage, b)
}

func testStoragePerformance(itemsInStorage int, b *testing.B) {
	s, _ := getStorage()
	keys := makeKeys(itemsInStorage)
	prepareStorage(&s, keys)
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_, err := s.GetString("333")

			if err != nil {
				b.Errorf("Failed reading from storage")
			}
		}
	})
}

func testStorage2Performance(itemsInStorage int, b *testing.B) {
	s, _ := getStorage2()
	keys := makeKeys(itemsInStorage)
	prepareStorage2(&s, keys)
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_, err := s.GetString("333")

			if err != nil {
				b.Errorf("Failed reading from storage")
			}
		}
	})
}

func testStorage3Performance(itemsInStorage int, b *testing.B) {
	s, _ := getStorage3()
	keys := makeKeys(itemsInStorage)
	prepareStorage3(&s, keys)
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_, err := s.GetString("333")

			if err != nil {
				b.Errorf("Failed reading from storage. Err: %v", err)
			}
		}
	})
	s.Storage.Close()
}
