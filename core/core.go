package core

import (
	_ "net/http/pprof"
	"net/rpc"
	"net"
	"github.com/spf13/viper"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/pr_grpc"
	"google.golang.org/grpc/reflection"
	"time"
	"strings"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/gorpc"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/util"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/di"
	"google.golang.org/grpc"
	"log"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/persistence"
	"os"
	"os/signal"
	"net/http"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/httpjson"
)

func Start() {

	initConfig()

	snapshotter := persistence.NewBinarySnapshotter(viper.GetString("snapshots.file"))
	loadSnapshot(snapshotter)
	go scheduleStorageSnapshots(snapshotter)
	scheduleStorageCleanup()

	initGorpcServer()
	initHttpServer()
	go initGrpcServer()

	onShutdown(snapshotter)
}

func loadSnapshot(snapshotter persistence.BinarySnapshotter) {
	di.ContainerLock()
	persistedStorage, err := snapshotter.Read()
	if err != nil {
		log.Fatalf("Failed reading to disk: %v", err)
	}
	di.ContainerUnlock()
	di.SetStorage(persistedStorage)
}

func onShutdown(snapshotter persistence.BinarySnapshotter) {
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt)

	<-shutdown
	makeSnapshot(snapshotter)
	log.Println("\nGraceful shutdown\n")
	os.Exit(0)
}

func scheduleStorageSnapshots(snapshotter persistence.BinarySnapshotter) {
	for {
		time.Sleep(time.Second * time.Duration(viper.GetInt("snapshots.make_every_seconds")))
		makeSnapshot(snapshotter)
	}
}

func makeSnapshot(snapshotter persistence.BinarySnapshotter) {
	di.ContainerLock()
	err := snapshotter.Write(di.GetStorage())
	if err != nil {
		log.Fatalf("Failed making snapshot: %v", err)
	}
	di.ContainerUnlock()
}

func initGorpcServer() {
	host := viper.GetString("server.gorpc:gob.host:port")

	logStarting("Starting gorpc:gob on " + host + "...\n")

	rpc.Register(new(gorpc.PsrStorage))

	goRpcListener, e := net.Listen("tcp", host)
	util.FailIfError(e, "Listen error")

	go rpc.Accept(goRpcListener)
}

func initHttpServer() {
	host := viper.GetString("server.http:json.host:port")

	logStarting("Starting http:json on " + host + "...\n")

	httpjson.InitHandlers()
	//httpListener, e := net.Listen("tcp", host)
	//util.FailIfError(e, "Listen error")

	server := http.Server{
		Addr: host,
	}
	go server.ListenAndServe()
	//go http.Serve(httpListener, nil)
}

func logStarting(str string) {
	util.LogIf(!util.IsTesting(), str)
}

func initGrpcServer() {
	host := viper.GetString("server.grpc:protobuf.host:port")

	logStarting("Starting grpc:protobuf on " + host + "...\n")

	listener, err := net.Listen("tcp", host)
	checkListenerError(err)

	grpc_server := grpc.NewServer()
	server := &pr_grpc.Server{Storage: di.GetStorage()}
	pr_grpc.RegisterPseudoRedisServiceServer(grpc_server, server)

	// Register reflection service on gRPC server.
	reflection.Register(grpc_server)
	err = grpc_server.Serve(listener)
	checkGrpcServeError(err)
}

func initConfig() {
	viper.SetConfigName("pr_config")  // name of config file (without extension)
	viper.AddConfigPath("/etc/")      // path to look for the config file in
	viper.AddConfigPath("$HOME/.psr") // call multiple times to add many search paths
	viper.AddConfigPath("../")        // optionally look for config in the working directory
	viper.AddConfigPath("./")         // optionally look for config in the working directory
	err := viper.ReadInConfig()       // Find and read the config file
	util.FailIfError(err, "Fatal error reading config file")
	di.MarkLoaded()
}

func scheduleStorageCleanup() {
	go func() {
		for {
			time.Sleep(time.Second * time.Duration(viper.GetInt("storage.cleanup_db_every_seconds")))
			storage := di.GetStorage()
			di.ContainerLock()
			storage.CleanUpExpired()
			di.ContainerUnlock()
		}
	}()
}

func checkListenerError(err error) {
	if err == nil {
		return
	}

	if util.IsTesting() && strings.Contains(err.Error(), "bind: address already in use") {
		log.Fatalf("Stop running server before running tests!")
	}

	log.Fatalf("failed to listen: %v", err)
}

func checkGrpcServeError(err error) {
	if err == nil {
		return
	}

	if util.IsTesting() && strings.Contains(err.Error(), "use of closed network connection") {
		return
	}

	log.Fatalf("failed to serve: %v", err)
}
