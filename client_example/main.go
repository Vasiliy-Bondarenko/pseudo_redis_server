package main

import (
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/prclient"
	"log"
	"fmt"
	"time"
)

func main() {
	host := "localhost:50052"
	maxConnections := 20

	client, err := prclient.New(host, maxConnections)
	if err != nil {
		log.Fatal("Can't connect to the server, Error: ", err)
	}

	res, err := client.Ping()
	fmt.Println(res) // Pong

	err = client.SetString("key", "value", time.Minute)
	if err != nil {
		log.Fatal("SetString() failed, got error: ", err)
	}

	value, err := client.GetString("key")
	if err != nil {
		log.Fatal("GetString() failed, got error: ", err)
	}
	fmt.Println(value) // "value"
}