package main

import (
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/core"
)

func main() {
	core.Start()
}
