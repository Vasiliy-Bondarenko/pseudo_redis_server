package prclient

import (
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/pr_grpc"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/gorpc"
	"time"
	"errors"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/pool"
)

type PrGoRpcClient struct {
	connectionPool *pool.PoolGoRpc
}

func New(host string, connection int) (PrGoRpcClient, error) {
	cp, err := pool.NewPoolGoRpc(host, connection)
	return PrGoRpcClient{
		connectionPool: cp,
	}, err
}

func (client PrGoRpcClient) SetList(key string, value []string, ttl time.Duration) error {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return err
	}

	req := gorpc.SetListRequest{
		Key:   key,
		Value: value,
		Ttl:   ttl,
	}
	var reply gorpc.SetReply
	err = rpc.Call("PsrStorage.SetList", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return err
	}
	if reply.Error {
		return errors.New(reply.ErrorMessage)
	}
	return nil
}

func (client PrGoRpcClient) GetString(key string) (string, error) {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return "", err
	}

	req := gorpc.GetRequest{
		Key: key,
	}
	var reply gorpc.GetStringReply
	err = rpc.Call("PsrStorage.GetString", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return reply.Value, err
	}
	if reply.Error {
		return reply.Value, errors.New(reply.ErrorMessage)
	}
	return reply.Value, nil
}

func (client PrGoRpcClient) GetList(key string) ([]string, error) {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return []string{}, err
	}

	req := gorpc.GetRequest{
		Key: key,
	}
	var reply gorpc.GetListReply
	err = rpc.Call("PsrStorage.GetList", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return reply.Value, err
	}
	if reply.Error {
		return reply.Value, errors.New(reply.ErrorMessage)
	}
	return reply.Value, nil
}

func (client PrGoRpcClient) SetString(key string, value string, ttl time.Duration) error {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return err
	}

	req := gorpc.SetStringRequest{
		Key:   key,
		Value: value,
		Ttl:   ttl,
	}
	var reply gorpc.SetReply
	err = rpc.Call("PsrStorage.SetString", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return err
	}
	if reply.Error {
		return errors.New(reply.ErrorMessage)
	}
	return nil
}

func (client PrGoRpcClient) Ping() (string, error) {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return "", err
	}

	var reply string
	err = rpc.Call("PsrStorage.Ping", pr_grpc.PingRequest{}, &reply)
	client.connectionPool.Put(rpc)
	return reply, err
}

func (client PrGoRpcClient) SetMap(key string, value map[string]string, ttl time.Duration) error {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return err
	}

	req := gorpc.SetMapRequest{
		Key:   key,
		Value: value,
		Ttl:   ttl,
	}
	var reply gorpc.SetReply
	err = rpc.Call("PsrStorage.SetMap", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return err
	}
	if reply.Error {
		return errors.New(reply.ErrorMessage)
	}
	return nil
}

func (client PrGoRpcClient) GetMap(key string) (map[string]string, error) {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return map[string]string{}, err
	}

	req := gorpc.GetRequest{
		Key: key,
	}
	var reply gorpc.GetMapReply
	err = rpc.Call("PsrStorage.GetMap", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return reply.Value, err
	}
	if reply.Error {
		return reply.Value, errors.New(reply.ErrorMessage)
	}
	return reply.Value, nil
}

func (client PrGoRpcClient) GetListItem(key string, index int64) (string, error) {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return "", err
	}

	req := gorpc.GetListItemRequest{
		Key:   key,
		Index: index,
	}
	var reply gorpc.GetListItemReply
	err = rpc.Call("PsrStorage.GetListItem", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return reply.Value, err
	}
	if reply.Error {
		return reply.Value, errors.New(reply.ErrorMessage)
	}
	return reply.Value, nil
}

func (client PrGoRpcClient) GetMapItem(key string, index string) (string, error) {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return "", err
	}

	req := gorpc.GetMapItemRequest{
		Key:   key,
		Index: index,
	}
	var reply gorpc.GetMapItemReply
	err = rpc.Call("PsrStorage.GetMapItem", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return reply.Value, err
	}
	if reply.Error {
		return reply.Value, errors.New(reply.ErrorMessage)
	}
	return reply.Value, nil
}

func (client PrGoRpcClient) Keys() ([]string, error) {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return []string{}, err
	}

	req := gorpc.GetKeysRequest{}
	var reply gorpc.GetKeysReply
	err = rpc.Call("PsrStorage.GetKeys", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return reply.Value, err
	}
	if reply.Error {
		return reply.Value, errors.New(reply.ErrorMessage)
	}
	return reply.Value, nil
}

func (client PrGoRpcClient) Remove(key string) (error) {
	rpc, err := client.connectionPool.Get()
	if err != nil {
		return err
	}

	req := gorpc.RemoveRequest{
		Key: key,
	}
	var reply gorpc.RemoveReply
	err = rpc.Call("PsrStorage.Remove", req, &reply)
	client.connectionPool.Put(rpc)
	if err != nil {
		return err
	}
	if reply.Error {
		return errors.New(reply.ErrorMessage)
	}
	return nil
}
