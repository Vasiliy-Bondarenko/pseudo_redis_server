package prclient

import (
	"net/http"
	"strings"
	"encoding/json"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/httpjson"
)

type HttpClient struct {
	http *http.Client
	host string
}

func NewHttpClient(host string, maxIdleConns int) HttpClient {
	return HttpClient{
		http: &http.Client{
			Transport: &http.Transport{
				MaxIdleConns:        maxIdleConns,
				MaxIdleConnsPerHost: maxIdleConns,
			},
		},
		host: host,
	}
}

func (client HttpClient) Ping() (string, error) {

	res, err := client.http.Post(client.url("/ping"), "application/json", strings.NewReader(""))
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	decoder := json.NewDecoder(res.Body)
	pingResponse := httpjson.PingResponse{}
	err = decoder.Decode(&pingResponse)
	if err != nil {
		return "", err
	}
	return pingResponse.Response, err
}

func (client HttpClient) url(url string) string {
	return "http://" + client.host + url
}
