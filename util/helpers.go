package util

import (
	"flag"
	"log"
)

func IsTesting() bool {
	return flag.Lookup("test.v") != nil
}

func FailIfError(err error, msg string, v ...interface{}) {
	if err != nil {
		log.Fatalf(msg+"\nError: "+err.Error(), v...)
	}
}

func LogIf(condition bool, msg string, v ...interface{}) {
	if condition {
		log.Printf(msg, v...)
	}
}

func LogIfError(err error, msg string, v ...interface{}) {
	LogIf(err != nil, msg, v...)
}

func HasError(err error) bool {
	return err != nil
}

func ErrorStr(err error) string {
	if err == nil {
		return ""
	}
	return err.Error()
}