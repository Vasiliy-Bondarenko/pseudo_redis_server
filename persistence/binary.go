package persistence

import (
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/storage"
	"os"
	"log"
	"bytes"
	"encoding/gob"
	"io"
	"github.com/jonboulle/clockwork"
	"errors"
	"fmt"
)

type BinarySnapshotter struct {
	filename string
}

func NewBinarySnapshotter(filename string) BinarySnapshotter {
	return BinarySnapshotter{
		filename: filename,
	}
}

func (snapshotter BinarySnapshotter) Read() (storage.Storage2, error) {
	file, closeFile := snapshotter.openFile()
	defer closeFile()

	newStorage := storage.NewStorage2(clockwork.NewRealClock())
	decoder := gob.NewDecoder(file)
	var i storage.KV
	for {
		err := decoder.Decode(&i)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal("decode error:", err)
		}
		newStorage.SetItem(i.Key, i.Item)
	}

	return newStorage, nil
}

func (snapshotter BinarySnapshotter) Write(storageInstance *storage.Storage2) error {

	file, err := os.Create(snapshotter.filename)
	if err != nil {
		return errors.New(fmt.Sprintf("Error creating file %v. Error: %v", snapshotter.filename, err))
	}
	defer file.Close()

	encoder := gob.NewEncoder(file)

	storageInstance.Range(func(key, value interface{}) bool {
		encoder.Encode(storage.KV{
			Key:  key.(string),
			Item: value.(storage.Item),
		})
		return true
	})

	err = file.Sync()
	if err != nil {
		return errors.New(fmt.Sprintf("Error syncing file %v. Error: %v", snapshotter.filename, err))
	}
	err = file.Close()
	if err != nil {
		return errors.New(fmt.Sprintf("Error closing file %v. Error: %v", snapshotter.filename, err))
	}

	return nil
}

func (snapshotter BinarySnapshotter) getBytes(data interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(data)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (snapshotter BinarySnapshotter) openFile() (*os.File, func()) {
	if _, err := os.Stat(snapshotter.filename); os.IsNotExist(err) {
		file, err := os.Create(snapshotter.filename)
		if err != nil {
			log.Fatalf("Can't create file: %v, error: %v", snapshotter.filename, err)
		}
		file.Close()
	}

	file, err := os.Open(snapshotter.filename)
	if err != nil {
		log.Fatalln("Can't open file: %v, error: %v", snapshotter.filename, err)
	}
	return file, func() {
		if err := file.Close(); err != nil {
			log.Fatalln(err)
		}
	}
}
