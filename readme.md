Task to solve: https://github.com/gojuno/test_tasks   
My solution: Pseudo Redis Client-Server

### Run server

    go run main.go

It will start two servers: 

* gorpc:gob on localhost:50052
* grpc:protobuf on localhost:50051 

### Client example

Look client_example/main.go

### TDD
Everything is "documented" in tests.

###Benchmark tests
    
    cd tests; go test -run=XXXXX -cpu=4 -benchtime=3s -v -bench=.

### Contacts
- vabondarenko@gmail.com
- skype: vasiliy.a.bondarenko
- tel: +66948031356
