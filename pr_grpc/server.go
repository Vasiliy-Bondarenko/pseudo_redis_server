package pr_grpc

import (
	"golang.org/x/net/context"
	"time"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/util"
	"bitbucket.org/Vasiliy-Bondarenko/pseudo_redis_server/storage"
)

type Server struct {
	Storage *storage.Storage2
}

// force to implement all methods of interface.
var _ PseudoRedisServiceServer = Server{}

func (s Server) Ping(ctx context.Context, req *PingRequest) (*PongResponse, error) {
	return &PongResponse{Message: "Pong"}, nil
}

func (s Server) SetString(ctx context.Context, req *SetStringRequest) (*SetResponse, error) {
	err := s.Storage.SetString(req.Key, req.Value, time.Duration(req.TtlInSeconds)*time.Second)

	resp := &SetResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
	}

	return resp, nil
}

func (s Server) GetString(ctx context.Context, req *GetStringRequest) (*GetStringResponse, error) {
	//defer profileFunction("GetString")()
	value, err := s.Storage.GetString(req.Key)

	resp := &GetStringResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
		Value:        value,
	}

	return resp, nil
}

func (s Server) SetList(ctx context.Context, req *SetListRequest) (*SetResponse, error) {
	err := s.Storage.SetList(req.Key, req.Value, time.Duration(req.TtlInSeconds)*time.Second)

	resp := &SetResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
	}

	return resp, nil
}

func (s Server) SetMap(ctx context.Context, req *SetMapRequest) (*SetResponse, error) {
	err := s.Storage.SetMap(req.Key, req.Value, time.Duration(req.TtlInSeconds)*time.Second)

	resp := &SetResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
	}

	return resp, nil
}

func (s Server) GetList(ctx context.Context, req *GetListRequest) (*GetListResponse, error) {
	value, err := s.Storage.GetList(req.Key)

	resp := &GetListResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
		Value:        value,
	}

	return resp, nil
}

func (s Server) GetMap(ctx context.Context, req *GetMapRequest) (*GetMapResponse, error) {
	value, err := s.Storage.GetMap(req.Key)

	resp := &GetMapResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
		Value:        value,
	}

	return resp, nil
}

func (s Server) GetValueFromList(ctx context.Context, req *GetValueFromListRequest) (*GetValueFromListResponse, error) {
	value, err := s.Storage.GetValueFromList(req.Key, req.Index)

	resp := &GetValueFromListResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
		Value:        value,
	}

	return resp, nil
}

func (s Server) GetValueFromMap(ctx context.Context, req *GetValueFromMapRequest) (*GetValueFromMapResponse, error) {
	value, err := s.Storage.GetValueFromMap(req.Key, req.Index)

	resp := &GetValueFromMapResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
		Value:        value,
	}

	return resp, nil
}

func (s Server) Remove(ctx context.Context, req *RemoveRequest) (*RemoveResponse, error) {
	err := s.Storage.Remove(req.Key)

	resp := &RemoveResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
	}

	return resp, nil
}

func (s Server) Keys(ctx context.Context, req *KeysRequest) (*KeysResponse, error) {
	keys, err := s.Storage.Keys()

	resp := &KeysResponse{
		Error:        util.HasError(err),
		ErrorMessage: util.ErrorStr(err),
		Keys:         keys,
	}

	return resp, nil
}
